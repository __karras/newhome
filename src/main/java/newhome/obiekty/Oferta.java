package newhome.obiekty;


public class Oferta implements Comparable<Oferta> {

    String nazwaOferty;
    Double Cena;
    Integer ID;
    Integer IDSprzedajacego;
    String typ;
    Integer metraz;
    Integer liczbaPomieszczen;
    String opis;

    public Oferta(String nazwaOferty, Double cena, Integer ID,Integer IDSprzedajacego, String typ, Integer metraz, Integer liczbaPomieszczen, String opis) {
        this.nazwaOferty = nazwaOferty;
        Cena = cena;
        this.ID = ID;
        this.IDSprzedajacego=IDSprzedajacego;
        this.typ = typ;
        this.metraz=metraz;
        this.liczbaPomieszczen=liczbaPomieszczen;
        this.opis=opis;

    }



    public String getNazwaOferty() {
        return nazwaOferty;
    }

    public void setNazwaOferty(String nazwaOferty) {
        this.nazwaOferty = nazwaOferty;
    }

    public Double getCena() {
        return Cena;
    }

    public void setCena(Double cena) {
        Cena = cena;
    }
    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public Integer getMetraz() {
        return metraz;
    }

    public void setMetraz(Integer metraz) {
        this.metraz = metraz;
    }

    public Integer getLiczbaPomieszczen() {
        return liczbaPomieszczen;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public void setLiczbaPomieszczen(Integer liczbaPomieszczen) {
        this.liczbaPomieszczen = liczbaPomieszczen;
    }
    public Integer getIDSprzedajacego() {
        return IDSprzedajacego;
    }

    public void setIDSprzedajacego(Integer IDSprzedajacego) {
        this.IDSprzedajacego = IDSprzedajacego;
    }

    @Override
    public int compareTo(Oferta o) {
        return this.getCena().compareTo(o.getCena());
    }
}
