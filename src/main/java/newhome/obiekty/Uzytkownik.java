package newhome.obiekty;

import java.util.ArrayList;

public class Uzytkownik {

    Integer ID;
    String nazwa;
    String haslo;
    ArrayList<Oferta> Obserwowane = new ArrayList<>();
    String email;
    String nrTel;

    public boolean czyObserwowana(Oferta x)
    {
        if(Obserwowane.contains(x))
            return true;
        else {
            return  false;
        }

    }
    public void dodajObserwowana(Oferta x)
    {
        Obserwowane.add(x); // tu można by zapisywać listę ofert do pliku / ciastka w celu jej odzyskania w przyszłosci
    }

    public void usunObserwowana(Oferta x)
    {
        Obserwowane.remove(x);
    }

    public Uzytkownik(Integer ID,String nazwa,String haslo,String email,String nrTel){
        this.ID=ID;
        this.nazwa = nazwa;
        this.haslo = haslo;
        this.email=email;
        this.nrTel=nrTel;
    }

    public String getNazwa() {
        return nazwa;
    }

    public String getHaslo() {
        return haslo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNrTel() {
        return nrTel;
    }

    public void setNrTel(String nrTel) {
        this.nrTel = nrTel;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public ArrayList<Oferta> getObserwowane() {
        return Obserwowane;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public void setHaslo(String haslo) {
        this.haslo = haslo;
    }

    public void setObserwowane(ArrayList<Oferta> obserwowane) {
        Obserwowane = obserwowane;
    }
}
