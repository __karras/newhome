package newhome.kontrolery;

import newhome.obiekty.Oferta;
import newhome.obiekty.Uzytkownik;

import javax.ejb.Singleton;
import java.util.ArrayList;


@Singleton(name = "UzytkownicyBean")
public class UzytkownicyBean {

    public ArrayList<Uzytkownik> uzytkownicy = new ArrayList<>();
    public Uzytkownik zalogowany = null;
    public  int nrZal = 0;
    String nazwa="";
    String haslo="";
    boolean czyZalogowany = false;

    public String getNazwa() {
        return nazwa;
    }

    public String getHaslo() {
        return haslo;
    }

    public boolean isCzyZalogowany() {
        return czyZalogowany;
    }


    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public void setHaslo(String haslo) {
        this.haslo = haslo;
    }

    public void setCzyZalogowany(boolean czyZalogowany) {
        this.czyZalogowany = czyZalogowany;
    }
    public boolean czyObserwowana(Oferta x){
        for (Uzytkownik usr : uzytkownicy)
        {
            if ( usr.getID() == nrZal)
            {
                return usr.czyObserwowana(x);
            }
        }
        return false;
    }
    public void dodajObserwowana(Oferta x){
        for (Uzytkownik usr : uzytkownicy)
        {
            if ( usr.getID() == nrZal)
            {
                usr.dodajObserwowana(x);
                return;
            }
        }
    }
    public void usunObserwowana(Oferta x)
    {
        for (Uzytkownik usr : uzytkownicy)
        {
            if ( usr.getID() == nrZal)
            {
                usr.usunObserwowana(x);
            }
        }
    }


    public UzytkownicyBean(){
        uzytkownicy.add(new Uzytkownik(1,"test", "test","test@email.com","956845369"));
        uzytkownicy.add(new Uzytkownik(2,"student", "wcy","student@email.com","694268525"));
        uzytkownicy.add(new Uzytkownik(3,"user", "Pal0Alt0","user@email.com","458983658"));
    }

    public Uzytkownik Zaloguj(String nazwa, String haslo)
    {
        for(int i=0; i<uzytkownicy.size();i++){
            Uzytkownik tmp = uzytkownicy.get(i);
            if(nazwa.compareTo(tmp.getNazwa())==0)
            {
                if (haslo.compareTo(tmp.getHaslo())==0){
                    zalogowany = tmp;
                    nrZal = i;
                    czyZalogowany = true;
                    return tmp;
                }
            }
        }
        return null;
    }

    public ArrayList<Uzytkownik> getUzytkownicy() {
        return uzytkownicy;
    }

    public void setUzytkownicy(ArrayList<Uzytkownik> uzytkownicy) {
        this.uzytkownicy = uzytkownicy;
    }

    public Uzytkownik wlascicielDanejOferty(Integer id)
    {
        for (Uzytkownik usr : uzytkownicy)
        {
            if (usr.getID().equals(id))
            {
                return usr;
            }
        }
        return null;
    }
}
