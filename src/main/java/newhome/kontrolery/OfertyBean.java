package newhome.kontrolery;

import newhome.obiekty.Oferta;
import newhome.obiekty.Uzytkownik;

import javax.ejb.Singleton;
import java.util.ArrayList;

@Singleton(name = "OfertyBean")
public class OfertyBean {


    ArrayList<String> typyOfert = new ArrayList<>();
    ArrayList<Oferta> oferty = new ArrayList<>();

    public OfertyBean() {

        typyOfert.add("mieszkanie");
        typyOfert.add("dom_wolnostojacy");

        oferty.add(new Oferta("Dom jednorodzinny Warszawa Włochy",500000.0,1,2,"dom_wolnostojacy",145,12, "\n" +
                "Mapa Strony\n" +
                "   Opis domu z poddaszem użytkowym 124,00 m2 (powierzchnia wg. podłóg - 145,50 m2)\tCopyright © 2003 by Domy Imperial  \n" +
                "Ekonomiczny funkcjonalny dom jednorodzinny z garażem i poddaszem użytkowym. Parter zajmuje nowoczesna, otwarta strefa dzienna z centralnie umieszczonym kominkiem. Przestronny, nasłoneczniony salon połączony z jadalnią, zlokalizowany jest po stronie południowo-zachodniej budynku, z panoramicznym wyjściem na otwarte patio, usadowione na odrębnych fundamentach o powierzchni 50 m2. Kuchnia usytuowana jest od strony północnej-zachodniej, przy wejściu do domu, z dostępem do hallu oraz jadalni. Z holu dostępna jest toaleta. Ponadto wiatrołap oraz kotłowania stanowią wygodną komunikację pomiędzy garażem a częścią mieszkalną. Na poddaszu mieszczą się trzy duże sypialnie oraz łazienka. Ze względu na gabaryty budowli, dom jest bardzo ekonomiczny w utrzymaniu. Prosta bryła budynku ozdobiona została stylowymi detalami - wykuszem oraz wejściem podkreślonym balkonem, wspartym na kolumnach.\n"));
        oferty.add(new Oferta("Mieszkanie na wynajem warszawa",2000.0,2,2,"mieszkanie",30,2,"To luksusowa kawalerka dla pary składająca się z pokoju dziennego z otwartym aneksem kuchennym, sypialni o powierzchni 14 m2, łazienki, holu z miejscem pod szafę zabudowaną lub garderobę i atrakcyjnego tarasu. Aneks kuchenny można wydzielić blatem lub ażurowym regałem, dzięki czemu zyska się jadalnię, miejsce do przechowywania albo interesującą dekorację.\n" +
                "\n" +
                "Taras przynależny do mieszkania zajmuje powierzchnię 9 m2 i ma atrakcyjny kształt. Wyjść na niego można z pokoju dziennego lub sypialni, a ponieważ jest częściowo zadaszony służyć będzie praktycznie przez cały rok. Zaletą apartamentu jest jego przestronność, mimo niewielkiego metrażu oraz funkcjonalność pomieszczeń, które łatwo się aranżuje i dostosowuje do potrzeb mieszkańców."));
        oferty.add(new Oferta("Kawalerka bemowo Warszawa", 14000.0,3,1,"mieszkanie",50,6,"Atrakcyjna kawalerka z sypialnią i pokojem dziennym połączonym z aneksem kuchennym. Wielką zaletą mieszkania jest przestronny hol z miejscem, które warto wykorzystać pod garderobę. Zarówno z pokoju dziennego, jak i sypialni można wyjść na taras o powierzchni 8 m2, który stanowić będzie przyjemny azyl dla mieszkańców. Aneks kuchenny usytuowany jest w taki sposób, by łatwo i atrakcyjnie wizualnie oddzielić się od pokoju dziennego, na przykład wysokim blatem i hokerami. W ten sposób zyskać można oryginalną jadalnię lub też stworzyć ażurowy regał na książki czy bibeloty."));
        oferty.add(new Oferta("Dom jednorodzinny Otwock", 115000.0,4,3,"dom_wolnostojacy",176,10,"Bardzo interesujący apartament o powierzchni 64 m2, w którym mieści się pokój dzienny z wydzielonym, przysłoniętym aneksem kuchennym, dwiema sypialniami, łazienką, holem. Do mieszkania przynależy ogródek, do którego wychodzi się wprost z pokoju dziennego lub z sypialni gospodarzy. Ogród to ogromna zaleta mieszkania, jego powierzchnia (w zależności od mieszkania 105 – 114 m2) jest spora i pozwala zorganizować przestrzeń do zabawy dla dzieci, wypoczynkową a nawet uprawiać zioła i warzywa na własny użytek. Część ogrodu jest zadaszona, dzięki czemu korzystać z niego można nawet przy gorszej pogodzie. Co ważne, ogród jest osłonięty przed wzrokiem przechodniów i pozwala właścicielom zachować pełnię prywatności.\n" +
                "\n" +
                "Pokój dzienny łączy się z aneksem kuchennym, jednak nie jest to otwarta przestrzeń. Dzięki temu rozwiązaniu kuchnia staje się praktycznie osobnym pomieszczeniem. Do aneksu kuchennego przylega niewielkie pomieszczenie gospodarcze o powierzchni 2,57 m2, które pełnić może funkcję schowka, suszarni, a nawet spiżarni. Sam pokój dzienny jest tak przestronny, że wykreować w nim można zarówno jadalnię, jak przestrzeń do wypoczynku i domowe biuro. Hol obejmujący 10 m2 daje możliwość postawienia w nim garderoby, stanowi też naturalne przedłużenie salonu, gdyż obie powierzchnie łączy dość szerokie przejście.\n"));
        oferty.add(new Oferta("Willa z basenem Ożarów Mazowiecki", 1100000.0,5,1,"dom_wolnostojacy",200,15,"Niezwykle komfortowe i przestronne mieszkanie zaplanowane dla rodziny z dwójką dzieci, która ceni sobie luksus. Mieszkanie podzielone jest na dwie strefy. Pierwsza to strefa dzienna, w której prowadzone jest życie rodzinne i towarzyskie, a składa się  na nią bardzo duży, bo obejmujący powierzchnię 46,4 m2, salon z jadalnią i aneksem kuchennym, toaleta oraz hol, w którym bez problemu zaaranżować można garderobę lub zabudowaną szafę. Strefa druga to część prywatna, na którą składają się trzy sypialnie, łazienka z toaletą, wanną i natryskiem oraz pomieszczenie gospodarcze będące jednocześnie pralnią i suszarnią.\n" +
                "\n" +
                "Niezwykle przestronny pokój dzienny oddziela do kuchni nowoczesna wyspa, dzięki której pani domu zyskuje dodatkową powierzchnię do przygotowywania posiłków oraz przechowywania. Widok z okien części dziennej to wyjątkowy rarytas. Z jadalni, przez niemal 4 metrową witrynę, wychodzi się na taras o powierzchni prawie 55 m2. Na tak dużej przestrzeni, częściowo zadaszonej, warto zorganizować miejsce do zabawy dla dzieci, kącik wypoczynkowy, a nawet hodowlę ziół, których zapach i smak doda nowej jakości życiu mieszkańców.\n" +
                "\n" +
                "Mieszkanie jest bardzo dobrze doświetlone i optymalnie zaplanowane. Ustawne sypialnie, pomieszczenie gospodarcze, toaleta w strefie dziennej sprawiają, że żyje się w nim bardzo komfortowo."));

    }

    public ArrayList<String> getTypyOfert() {
        return typyOfert;
    }

    public void setTypyOfert(ArrayList<String> typyOfert) {
        this.typyOfert = typyOfert;
    }

    public ArrayList<Oferta> getOferty() {
        return oferty;
    }

    public void setOferty(ArrayList<Oferta> oferty) {
        this.oferty = oferty;
    }

    public Oferta getOferta(Integer ID){
        Oferta tmp;
        for(int i=0;i<oferty.size();i++){
            tmp = oferty.get(i);
            if(tmp.getID().equals(ID))
                return tmp;
        }
        return null;
    }

    public ArrayList<Oferta> wyszukajOferty(String searchPhrase)
    {
        if (searchPhrase.equals("") || searchPhrase.equals(null))
        {
            return this.oferty;
        }
        else
        {
            ArrayList<Oferta> returnList = new ArrayList<>();
            for (Oferta oft : oferty)
            {
                if(oft.getNazwaOferty().toLowerCase().contains(searchPhrase.toLowerCase()))
                {
                    returnList.add(oft);
                }
            }

            return returnList;
        }
    }

}
