package newhome.interfejsy;

import newhome.kontrolery.OfertyBean;
import newhome.obiekty.Oferta;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class InterfejsWyszukiwania {

    @EJB
    OfertyBean ofertyBean;

    String searchPhrase="";
    String searchPhraseForFiltr;
    String filtrCeny;
    String filtrTypu;

    private ArrayList<Oferta> wyszukaneOferty = new ArrayList<>();
    private ArrayList<Oferta> filtrowaneOferty = new ArrayList<>();

    public InterfejsWyszukiwania()
    {

    }

    @PostConstruct
    public void init()
    {

    }

    public String getFiltrTypu() {
        return filtrTypu;
    }

    public void setFiltrTypu(String filtrTypu) {
        this.filtrTypu = filtrTypu;
    }

    public String getSearchPhraseForFiltr() {
        return searchPhraseForFiltr;
    }

    public void setSearchPhraseForFiltr(String searchPhraseForFiltr) {
        this.searchPhraseForFiltr = searchPhraseForFiltr;
    }

    public ArrayList<Oferta> getFiltrowaneOferty() {
        return filtrowaneOferty;
    }

    public void setFiltrowaneOferty(ArrayList<Oferta> filtrowaneOferty) {
        this.filtrowaneOferty = filtrowaneOferty;
    }

    public String getFiltrCeny() {
        return filtrCeny;
    }

    public void setFiltrCeny(String filtrCeny) {
        this.filtrCeny = filtrCeny;
    }

    public ArrayList<Oferta> getWyszukaneOferty() {
        return wyszukaneOferty;
    }

    public void setWyszukaneOferty(ArrayList<Oferta> wyszukaneOferty) {
        this.wyszukaneOferty = wyszukaneOferty;
    }

    public String getSearchPhrase() {
        return searchPhrase;
    }

    public void setSearchPhrase(String searchPhrase) {
        this.searchPhrase = searchPhrase;
    }

    public ArrayList<String> typyOfert()
    {
        return ofertyBean.getTypyOfert();
    }
    public String wyszukiwanie()
    {
        wyszukaneOferty = ofertyBean.wyszukajOferty(searchPhrase);
        filtrowaneOferty = (ArrayList<Oferta>) wyszukaneOferty.clone();
        searchPhraseForFiltr = searchPhrase;
        return "wyszukiwanie.xhtml?faces-redirect=true";
    }

    public void filtrowanie()
    {
        if(!searchPhraseForFiltr.equals(searchPhrase))
        {
            wyszukaneOferty = ofertyBean.wyszukajOferty(searchPhraseForFiltr);
            searchPhrase = searchPhraseForFiltr;
        }

        filtrowaneOferty = (ArrayList<Oferta>) wyszukaneOferty.clone();
        if(filtrCeny.equals("od_najwyzszej"))
        {
            filtrowaneOferty.sort(Collections.reverseOrder());
        }
        else if(filtrCeny.equals("od_najnizszej"))
        {
            Collections.sort(filtrowaneOferty);
        }

        if(filtrTypu.equals("mieszkanie"))
        {
            filtrowaneOferty.removeIf(oft -> !oft.getTyp().equals("mieszkanie"));
        }
        else if(filtrTypu.equals("dom_wolnostojacy"))
        {
            filtrowaneOferty.removeIf(oft -> !oft.getTyp().equals("dom_wolnostojacy"));
        }

    }
}
