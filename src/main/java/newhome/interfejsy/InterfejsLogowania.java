package newhome.interfejsy;

import newhome.kontrolery.OfertyBean;
import newhome.kontrolery.UzytkownicyBean;
import newhome.obiekty.Oferta;
import newhome.obiekty.Uzytkownik;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;


public class InterfejsLogowania {

    @EJB
    UzytkownicyBean uzytkownicyBean;

    public String getNazwatmp() {
        return nazwatmp;
    }

    public String getHaslotmp() {
        return haslotmp;
    }

    String nazwatmp="";

    public void setNazwatmp(String nazwatmp) {
        this.nazwatmp = nazwatmp;
    }

    public void setHaslotmp(String haslotmp) {
        this.haslotmp = haslotmp;
    }

    String haslotmp="";
    Uzytkownik tmp;
    boolean zal=false;

    public void setZal(boolean zal) {
        this.zal = zal;
    }

    public boolean isZal() {
        return zal;
    }

    public void setKomunikat(String komunikat) {
        this.komunikat = komunikat;
    }

    public String getKomunikat() {
        return komunikat;
    }

    String komunikat="Zaloguj się";

    public InterfejsLogowania()
    {

    }
    @PostConstruct
    public void init()
    {

    }
    public void reload() throws IOException {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
    }
    public void zaloguj() throws IOException
    {
        tmp = uzytkownicyBean.Zaloguj(nazwatmp,haslotmp);
        if(tmp!=null)
        {
            zal = true;
            System.out.println("Udalo się");
            komunikat = tmp.getNazwa();
            System.out.println(komunikat);
            //ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            //ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());

        }
        else
            {
                System.out.println("Nie Udalo się");
            }

    }

}

