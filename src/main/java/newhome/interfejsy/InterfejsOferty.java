package newhome.interfejsy;

import newhome.kontrolery.OfertyBean;
import newhome.kontrolery.UzytkownicyBean;
import newhome.obiekty.Oferta;
import newhome.obiekty.Uzytkownik;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;

public class InterfejsOferty {

    @EJB
    OfertyBean ofertyBean;

    @EJB
    UzytkownicyBean uzytkownicyBean;

    String nazwa;
    Double cena;
    Integer id;

    private Oferta wybranaOferta;
    private Uzytkownik sprzedajacy;

    public InterfejsOferty()
    {

    }

    @PostConstruct
    public void init()
    {
        id = Integer.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("ID"));
        wybranaOferta = ofertyBean.getOferta(id);
        nazwa = wybranaOferta.getNazwaOferty();
        cena = wybranaOferta.getCena();
        sprzedajacy = uzytkownicyBean.wlascicielDanejOferty(wybranaOferta.getIDSprzedajacego());
    }

    public void dodajObserwowane(){

        if(uzytkownicyBean.isCzyZalogowany()){

            if(uzytkownicyBean.czyObserwowana(wybranaOferta))
            {
                System.out.println("Oferta jest już w obserwowanych");
            }
            else
            {
                System.out.println("dodałem");
                uzytkownicyBean.dodajObserwowana(wybranaOferta);
            }
        }
        else{
            System.out.println("Niezalogowany uzytkownik"); // tu można przekierować do logowania????
        }

    }

    public void usunObserwowane()
    {
        uzytkownicyBean.usunObserwowana(wybranaOferta);
    }

    public Oferta getWybranaOferta() {
        return wybranaOferta;
    }

    public Double getCena()
    {
        return wybranaOferta.getCena();
    }
    public String getNazwa(){
        return wybranaOferta.getNazwaOferty();
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public void setCena(Double cena) {
        this.cena = cena;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setWybranaOferta(Oferta wybranaOferta) {

        this.wybranaOferta = wybranaOferta;
    }

    public Uzytkownik getSprzedajacy() {
        return sprzedajacy;
    }

    public void setSprzedajacy(Uzytkownik sprzedajacy) {
        this.sprzedajacy = sprzedajacy;
    }

    public boolean renderObsButton()
    {
        return uzytkownicyBean.isCzyZalogowany();

    }
    public boolean renderIfObserwowane()
    {
        return uzytkownicyBean.czyObserwowana(wybranaOferta);
    }
}